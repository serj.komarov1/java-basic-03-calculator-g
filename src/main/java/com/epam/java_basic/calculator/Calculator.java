package com.epam.java_basic.calculator;

import static java.lang.Double.NEGATIVE_INFINITY;
import static java.lang.Double.POSITIVE_INFINITY;

public class Calculator {
    private static final int INDEX_MATH = 10;
    private int precision;

    public Calculator(Integer precision) {
        this.precision = precision;
    }

    public double add(double a, double b) {
        Double result = getPrecision((a + b));
        return result;
    }

    public double subtract(double a, double b) {
        Double result = getPrecision((a - b));
        return result;
    }

    public double multiply(double a, double b) {
        Double result = getPrecision((a * b));
        return result;
    }

    public double div(double a, double b) {
        Double result = getPrecision((a / b));
        return result;

    }

    public double getPrecision(Double a) {
        if (a == POSITIVE_INFINITY || a == NEGATIVE_INFINITY) {
            return a;
        } else {
            double scale = Math.pow(INDEX_MATH, precision);
            Double result = Math.round(a * scale) / scale;
            return result;
        }

    }
}


