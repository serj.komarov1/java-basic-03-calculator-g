package com.epam.java_basic;

public class View {
    public static final String MESSAGE_ENTER_THE= "Enter the ";
    public static final String MESSAGE_NUMBER = " number: \n";
    public static final String MESSAGE_FIRST = "first";
    public static final String MESSAGE_SECOND = "second";
    public static final String MESSAGE_WRONG_FORMAT = "Wrong number format. Try again. \n";
    public static final String MESSAGE_ENTER_OPERATOR =  "Enter operator (+, -, *, /): \n";
    public static final String MESSAGE_ENTER_INCORRECT_CHOICE ="Incorrect choice! Try again \n";
    public static final String MESSAGE_ENTER_DIV_BY_NOL = "На ноль делить нельзя";
    public static final String OPERATION_PLUS = "+";
    public static final String OPERATION_MINUS = "-";
    public static final String OPERATION_MYLTI = "*";
    public static final String OPERATION_DIV = "/";
    public void printMessage(String a) {
        System.out.print(a);
    }
    public void printDouble(Double a) {
        System.out.println(a);
    }

}
