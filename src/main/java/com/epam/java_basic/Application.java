package com.epam.java_basic;

import com.epam.java_basic.calculator.Calculator;

import java.util.Scanner;

/**
 * Application's entry point, use it to demonstrate your code execution
 */
public class Application {
    Scanner sc = new Scanner(System.in);
    private static final int DEFAULT_PRESICION = 4;
    public static final int PRESICION_INDEX = 0;
    private final View view;
    private final Calculator calculator;

    public Application(String[] a) {
        calculator = new Calculator(getPresicion(a));
        view = new View();
    }

    private Integer getPresicion(String[] a) {
        try {
            return Integer.parseInt(a[PRESICION_INDEX]);

        } catch (Exception e) {
            return DEFAULT_PRESICION;
        }
    }

    public static void main(String[] args) {
        Application application = new Application(args);
        application.start();

    }

    public void start() {

        getOperation(sc, getNumber(View.MESSAGE_FIRST), getNumber(View.MESSAGE_SECOND));

    }

    private Double getNumber(String numberPos) {
        Double number;
        do {
            view.printMessage(View.MESSAGE_ENTER_THE);
            view.printMessage(numberPos);
            view.printMessage(View.MESSAGE_NUMBER);
            number = getNum(sc);
        } while (number == null);
        return number;
    }

    private Double getNum(Scanner sc) {
        String number = sc.nextLine();
        try {
            return Double.parseDouble(number);

        } catch (Exception e) {
            view.printMessage(View.MESSAGE_WRONG_FORMAT);
            return null;
        }

    }

    private void getOperation(Scanner sc, double a, double b) {
        view.printMessage(View.MESSAGE_ENTER_OPERATOR);
        String op = sc.next();

        if (View.OPERATION_PLUS.equals(op)) {
            view.printDouble(calculator.add(a, b));


        } else if (View.OPERATION_MINUS.equals(op)) {
            view.printDouble(calculator.subtract(a, b));

        } else if (View.OPERATION_MYLTI.equals(op)) {
            view.printDouble(calculator.multiply(a, b));

        } else if (View.OPERATION_DIV.equals(op)) {
            view.printDouble(calculator.div(a, b));

        } else {
            view.printMessage(View.MESSAGE_ENTER_INCORRECT_CHOICE);
            getOperation(sc, a, b);
        }
    }
}


